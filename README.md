<h1>About this project</h1>
Hi! 
This application downloads tasks from https://jsonplaceholder.typicode.com/todos/. After downloading, you can add your tasks to the end of the list by entering the value in the form and click the 'add' button. Also edit the task by clicking on the 'edit' button and delete the task by clicking on the 'delete' button. 

<h2>To deploy an application locally, you can:</h2>

### `git clone https://gitlab.com/andrew_florida/react-redux-thunk.git`
### `npm install`
### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

<h2>File structure:</h2>
<pre class="code highlight js-syntax-highlight plaintext white" lang="plaintext" v-pre="true"><code>
├── public/                      
│   ├── index.html            
├── src/ 
│   ├── actions/                    
│      ├── items.js                          
│   ├── components/             
│      ├── add-todo/        
│      ├── todo/          
│      ├── todo-list.js               
│   ├── reducers/             
│      ├── index.js
│      ├── items.js  
│      ├── todo-list.js
│   ├── store/             
│      ├── configureStore.js                                  
|   ├── index.js/               
├── .gitignore           
├── package.json         
<code></pre>

