import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { itemsFetchData, onEdit, onChange, onDelete } from '../actions/items';
import AddTodo from './AddTodo';
import Todo from './Todo'

const mapStateToProps = (state) => {
    return {
        items: state.items,
        hasError: state.itemsHasError,
        isLoading: state.itemsIsLoading,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: (url) => dispatch(itemsFetchData(url)),
        ...bindActionCreators({onChange, onDelete, onEdit }, dispatch)
    };
};

class TodoList extends Component {
    componentDidMount() {
        this.props.fetchData('https://jsonplaceholder.typicode.com/todos/');
    }

    render() {
        if (this.props.hasError) {
            return <p>Sorry! There was an error loading the items</p>;
        }

        if (this.props.isLoading) {
            return <p>Loading…</p>;
        }

        return (
            <div className="todos" >
                <AddTodo />
                <ul>
                    {this.props.items.map((item) => (
                        <Todo
                            title={item.title}
                            key={item.id}
                            edit={item.edit}
                            onDelete={() => this.props.onDelete(item.id)}
                            onChange={(e) => this.props.onChange(e.target.value)}
                            onEdit={() => this.props.onEdit(item.id)}
                        />
                    ))}
                </ul>
            </div>
        );
    }

}

TodoList.propTypes = {
    fetchData: PropTypes.func.isRequired,
    items: PropTypes.array.isRequired,
    hasError: PropTypes.bool.isRequired,
    isLoading: PropTypes.bool.isRequired,
    onEdit: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(TodoList);

