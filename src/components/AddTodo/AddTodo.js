import React, { Component } from 'react'
import { connect } from 'react-redux'

import { addTodo } from '../../actions/items'
import './AddTodo.css';

class AddTodo extends Component {
    
    input = null

    render(){
        return (
            <form
                onSubmit={e => {
                    e.preventDefault()
                    let comment = this.input.value;
                    if (!comment.trim()) {
                        return
                    }
                    this.props.dispatch(addTodo(comment))
                    
                    this.input.value = ''
                }}>
                <input type="text"
                    defaultValue={''}
                    ref={node => this.input = node}
                    placeholder="Create task" />

                <button type="submit">Add</button>
            </form>
        );
    }
    
}

export default connect()(AddTodo)