export const itemsHasError = (state = false, action) => {
    switch (action.type) {
        case 'ITEMS_HAS_ERRORED':
            return action.hasError;

        default:
            return state;
    }
}

export const itemsIsLoading = (state = false, action) => {
    switch (action.type) {
        case 'ITEMS_IS_LOADING':
            return action.isLoading;

        default:
            return state;
    }
}

export const items = (items = [], action) => {
    switch (action.type) {
        case 'ITEMS_FETCH_DATA_SUCCESS':
            return action.items;

        case 'ADD_TODO':
            return [
                {
                    id: action.id,
                    title: action.title,
                    edit: false
                },
                ...items
            ]

        case 'EDIT_TODO':
            return items.map(item => (item.id === action.id) ? { ...item, edit: !item.edit } : item)
/***/
        case 'CHANGE_TODO':
            //return items ? items.filter((item) => item.id !== action.id) : []
            return items.map(item => (item.id === action.id) ? { ...item, title: action.title } : item)
/***/
        case 'SHOUTOUT_DELETED':
            return items ? items.filter((item) => item.id !== action.id) : []

        default:
            return items
    }
}