//import { bindActionCreators } from "redux";

let nextTodoId = 201

export const addTodo = title => ({
    type: 'ADD_TODO',
    id: nextTodoId++,
    title
})

export const onEdit = id => ({
    type: 'EDIT_TODO',
    id
})

export const onDelete = id => ({
    type: 'SHOUTOUT_DELETED',
    id
})

export const onChange = e => ({
    type: 'CHANGE_TODO',
    title: e, 
})

export const itemsHasError = bool => ({
    type: 'ITEMS_HAS_ERRORED',
    hasError: bool
})

export const itemsIsLoading = bool => ({
    type: 'ITEMS_IS_LOADING',
    isLoading: bool
})

export const itemsFetchDataSuccess = items => ({
    type: 'ITEMS_FETCH_DATA_SUCCESS',
    items
})

export const itemsFetchData = url => {
    return (dispatch) => {
        dispatch(itemsIsLoading(true));

        fetch(url)
            .then((response) => {
                if (!response.ok) {
                    throw Error(response.statusText);
                }

                dispatch(itemsIsLoading(false));

                return response;
            })
            .then((response) => response.json())
            .then((items) => dispatch(itemsFetchDataSuccess(items)))
            .catch(() => dispatch(itemsHasError(true)));
    };
}
